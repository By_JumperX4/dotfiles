# dotfiles
My dotfiles. Fell free to use them and fork them.

Warning : commented with ass and partially in frenglish (french + english). May be impossible to understand.

You can get my Firefox Color theme [here](https://color.firefox.com/?theme=XQAAAAIfAwAAAAAAAABBqYhm849SCicxcUN7ViuG_ebZUZXOFqkBopaPs6h5TfgsoKxLuWMWY3dHRCnsX-7EkSmucBB2AWcEXCjU4E0T9-zjakWeQXyWKme8vepZBsin-1UD6p3850ufIL16g7AkpWyvms-NUO7ROlLXLGtO51zsyc6IpTRZ37xwRfr8j5weN06mif80KXrzqBetU9nKwG1Q9eJy94VOeNxnxYbvfN-0GEqNYsoR8Wxbkx65hgFhEOE2yFC7f10ATBtvrCocWfGw_um7PBhXCfBX6fm2w0fZzqyAaTBE8hsnzau0JgpAIEY7HIGsdZ0zE1fvDiWe9cdRXuarNRnZCtpAncdY2DEtnjlcGXXCOwSqkgCnrs-BFs84z6Tpmyq1Wqig-SzTIp9MxHDnkKND7idR2mNsli_Pe9S8D50jA-PmzgVNrwPnbsqB5b6NEbW5j5QrEcyNkRrYGCcsvp97_qjx1ptpbyO2P_7lJzw). Works with Tor Browser too, you need the Firefox Color extension from Mozilla.


My setup :

Distro : Gentoo Hardened OpenRC SELinux + ClamAV Root-On-ZFS

CPUs : 2x Intel Xeon X5680

GPU : PNY nVidia GTX 1060 6GB

GPU Driver : nVidia proprietary driver

RAM : 108 GB of ram (DDR3)

Locale : fr_BE.UTF-8

Keymap : us-altgr-intl

Screens setup :

1x vertical 1920x1080 screen (left)

1x horizontal 2560x1440 screen (middle)

1x horizontal 1360x768 screen (right)

1x horizontal 1920x1080 graphics tablet (in front of the middle screen, middle screen is higher so it doesn't mask the screen)

DE/WM : MATE + i3 in floating mode

Editor : Emacs

Terminal Emulator : XTerm

Shell : fish as interactive shell, autostarted with bash